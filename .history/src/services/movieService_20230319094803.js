import axios from 'axios';
import { BASE_URL, configHeaders } from './config';

export const movieService = {
  getMovieList: () => {
    return axios({
      url: `${BASE_URL}/api/QuanLyPhim/LayDanhSachPhim?maNhom=GP04`,
      method: 'GET',
      headers: configHeaders(),
    });
  },
  getMoiveByTheater: () => {
    return axios({
      url: `${BASE_URL}/api/QuanLyRap/LayThongTinLichChieuHeThongRap?maNhom=GP04`,
      method: 'GET',
      headers: configHeaders(),
    });
  },
};
