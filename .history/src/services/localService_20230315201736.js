export const USER_LOGIN = 'USER_LOGIN';
export const localUserService = () => {
  get: () => {};
  set: (userInfo) => {
    let jsonData = JSON.stringify(userInfo);
    localStorage.setItem(USER_LOGIN, jsonData);
  };
};
