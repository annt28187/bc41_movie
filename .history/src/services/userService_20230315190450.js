import axios from 'axios';
import { configHeaders } from './config';

export const userService = {
  login: (loginData) => {
    return axios({
      url: `&{BASE_URL}//api/QuanLyNguoiDung/DangNhap`,
      method: 'POST',
      data: loginData,
      headers: configHeaders(),
    });
  },
};

userService.login();
