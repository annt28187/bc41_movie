import React from 'react';
import Header from '../Components/Header/Header';
import Footer from './../Components/Footer/Footer';

export default function Layout({ Component }) {
  return (
    <div className="space-y-10">
      <Header />
      <Component />
      <Footer />
    </div>
  );
}
