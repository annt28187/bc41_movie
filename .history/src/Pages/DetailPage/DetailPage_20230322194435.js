import React, { useEffect, useState } from 'react';
import { useParams } from 'react-router-dom';
import { movieServ } from '../../services/movieService';
import { Progress } from 'antd';

export default function DetailPage() {
  let params = useParams();
  let [movie, setMovie] = useState({});
  useEffect(() => {
    let fetchDetail = async () => {
      try {
        let result = await movieServ.getDetailMoive(params.id);
        setMovie(result.data.content);
      } catch (error) {}
    };
    fetchDetail();
  }, []);

  return (
    <div className="container flex">
      <img className="w-1/4" src={movie.hinhAnh} alt="" />
      <div className="p-5">
        <h3 className="font-bold text-red-600">{movie.tenPhim}</h3>
        <p>{movie.moTa}</p>
        <Progress percent={movie.danhGia * 10} />
      </div>
    </div>
  );
}
