import React, { useEffect, useState } from 'react';
import { useParams } from 'react-router-dom';
import { movieServ } from '../../services/movieService';

export default function DetailPage() {
  let params = useParams();
  let [movie, setMovie] = useState();
  useEffect(() => {
    let fetchDetail = async () => {
      try {
        let res = await movieServ.getDetailMoive(params.id);
      } catch (error) {}
    };
    fetchDetail();
  }, []);

  return <div>DetailPage</div>;
}
