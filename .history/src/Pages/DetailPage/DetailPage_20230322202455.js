import React, { useEffect, useState } from 'react';
import { NavLink, useParams } from 'react-router-dom';
import { movieServ } from '../../services/movieService';
import { Progress } from 'antd';

export default function DetailPage() {
  let params = useParams();
  let [movie, setMovie] = useState({});
  useEffect(() => {
    let fetchDetail = async () => {
      try {
        let result = await movieServ.getDetailMoive(params.id);
        setMovie(result.data.content);
      } catch (error) {}
    };
    fetchDetail();
  }, []);

  return (
    <div className="container flex">
      <img className="w-1/4" src={movie.hinhAnh} alt="" />
      <div className="p-5 space-y-10">
        <h3 className="text-xl font-medium text-red-600">{movie.tenPhim}</h3>
        <p className="text-xs text-gray-600">{movie.moTa}</p>
        <Progress
          type="circle"
          percent={movie.danhGia * 10}
          format={(percent) => `${percent / 10} Điểm`}
        />
        <NavLink
          to={`/booking/${movie.maPhim}`}
          className="px-5  py-2 rounded bg-red-500 text-white"
        >
          Mua vé
        </NavLink>
      </div>
    </div>
  );
}
