import React from 'react';
import TabMovie from '../TabMovie/TabMovie';
import ListMovie from './ListMovie/ListMovie';

export default function HomePage() {
  return (
    <div className="space-y-10">
      <ListMovie />
      <TabMovie />
    </div>
  );
}
