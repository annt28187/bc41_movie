import React from 'react';
import { Button, Card } from 'antd';
const { Meta } = Card;

export default function ItemMovie({ data }) {
  return (
    <Card
      hoverable
      style={{
        width: '100%',
      }}
      cover={<img alt={data.tenPhim} src={data.hinhAnh} />}
    >
      <Button danger type="primary">
        Xem ngay
      </Button>
    </Card>
  );
}
