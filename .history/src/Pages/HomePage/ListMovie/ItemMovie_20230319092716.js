import React from 'react';
import { Button, Card } from 'antd';
const { Meta } = Card;

export default function ItemMovie({ data }) {
  return (
    <Card
      hoverable
      style={{
        width: '100%',
      }}
      cover={<img className="h-60 object-cover object-top" alt={data.tenPhim} src={data.hinhAnh} />}
    >
      <Button danger type="primary">
        Xem ngay
      </Button>
    </Card>
  );
}
