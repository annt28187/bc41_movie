import React from 'react';
import { Card } from 'antd';
const { Meta } = Card;

export default function ItemMovie({ data }) {
  return (
    <Card
      hoverable
      style={{
        width: '100%',
      }}
      cover={<img alt="example" src={data.hinhAnh} />}
    >
      <Meta title={data.tenPhim} description={data.moTa} />
    </Card>
  );
}
