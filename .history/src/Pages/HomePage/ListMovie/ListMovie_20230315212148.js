import React, { useEffect, useState } from 'react';
import { movieService } from '../../../services/movieService';

export default function ListMovie() {
  useEffect(() => {
    const [movieList, setMoiveList] = useState([]);
    movieService
      .getMovieList()
      .then((res) => {
        console.log(res.data.content);
        setMoiveList(res.data.content);
      })
      .then((err) => {
        console.log(err);
      });
  });
  return <div>ListMovie</div>;
}
