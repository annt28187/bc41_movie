import React, { useEffect, useState } from 'react';
import { movieService } from '../../../services/movieService';
import ItemMovie from './ItemMovie';

export default function ListMovie() {
  useEffect(() => {
    const [movieList, setMoiveList] = useState([]);
    movieService
      .getMovieList()
      .then((res) => {
        console.log(res.data.content);
        setMoiveList(res.data.content);
      })
      .then((err) => {
        console.log(err);
      });
  });
  return (
    <div className="gird grid-cols-4 gap-10">
      {movieList.map((item) => {
        return <ItemMovie data={item} key={item.maPhim} />;
      })}
    </div>
  );
}
