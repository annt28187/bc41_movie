import React, { useEffect } from 'react';
import { movieService } from '../../../services/movieService';

export default function ListMovie() {
  useEffect(() => {
    movieService
      .getMovieList()
      .then((res) => {
        console.log(res.data.content);
      })
      .then((err) => {
        console.log(err);
      });
  });
  return <div>ListMovie</div>;
}
