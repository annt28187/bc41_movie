import React from 'react';
import Header from '../../Components/Header/Header';
import ListMovie from './ListMovie/ListMovie';

export default function HomePage() {
  return (
    <div className="container mx-auto">
      <Header />
      <ListMovie />
    </div>
  );
}
