import React, { useEffect } from 'react';
import { movieServ } from '../../services/movieService';
import { Tabs } from 'antd';

const onChange = (key) => {
  console.log(key);
};
const items = [
  {
    key: '1',
    label: `Tab 1`,
    children: `Content of Tab Pane 1`,
  },
];
export default function TabMovie() {
  const [danhSachHeThongRap, setDanhSachHeThongRap] = useEffect(() => {
    movieServ
      .getMoiveByTheater()
      .then((res) => {
        console.log(res.data.content);
        setDanhSachHeThongRap(res.data.content);
      })
      .catch((err) => {
        console.log(err);
      });
  });
  let renderHeThongRap = () => {
    return danhSachHeThongRap.map((heThongRap) => {
      return {
        key: heThongRap.maHeThongRap,
        label: <img className="h-16" src={heThongRap.logo} alt="" />,
        children: `Content of Tab Pane 1`,
      };
    });
  };
  return (
    <div className="container">
      <Tabs
        tabPosition="left"
        defaultActiveKey="1"
        items={renderHeThongRap()}
        onChange={onChange}
      />
      ;
    </div>
  );
}
