import React from 'react';

export default function ItemTabMovie({ phim }) {
  return (
    <div className="flex">
      <img src={phim.hinhAnh} className="w-28" />
      <div>
        <h5 className="text-2xl">{phim.tenPhim}</h5>
      </div>
    </div>
  );
}
