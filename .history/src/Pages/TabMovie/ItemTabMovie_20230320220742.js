import moment from 'moment/moment';
import React from 'react';

export default function ItemTabMovie({ phim }) {
  return (
    <div className="flex">
      <img src={phim.hinhAnh} className="w-28" alt="" />
      <div>
        <h5 className="text-2xl font-medium">{phim.tenPhim}</h5>
        <div className="gird gap-5 gird-cols-3">
          {phim.lstLichChieuTheoPhim.map((item) => {
            return (
              <span className="rounded p-3 text-white bg-red-500">
                {moment(item.ngayChieuGioChieu).format('L')}
              </span>
            );
          })}
        </div>
      </div>
    </div>
  );
}
