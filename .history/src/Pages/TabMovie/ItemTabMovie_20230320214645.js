import React from 'react';

export default function ItemTabMovie({ phim }) {
  return (
    <div className="flex">
      <img src={phim.hinhAnh} className="w-28" alt="" />
      <div>
        <h5 className="text-2xl font-medium">{phim.tenPhim}</h5>
        <div>
          {phim.lstLichChieuTheoPhim.map((item) => {
            return <span>{item.ngayChieuGioChieu}</span>;
          })}
        </div>
      </div>
    </div>
  );
}
