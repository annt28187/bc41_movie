import { Button, Form, Input, message } from 'antd';
import { useDispatch } from 'react-redux';
import { useNavigate } from 'react-router-dom';
import { SET_USER_LOGIN } from '../../redux/constants/userConstant';
import { localUserServ } from '../../services/localService';
import { userServ } from '../../services/userService';
import Lottie from 'lottie-react';
import bg_animate from '../../assets/animate_login.json';
import { setLoginAction, setLoginActionService } from './../../redux/actions/userAction';
const LoginPage = () => {
  let navigate = useNavigate();
  let dispatch = useDispatch();
  const onFinish = (values) => {
    userServ
      .login(values)
      .then((res) => {
        message.success('Đăng nhập thành công');
        //Lưu thông tin user xuống localStorage
        localUserServ.set(res.data.content);
        // dispatch({
        //   type: SET_USER_LOGIN,
        //   payload: res.data.content,
        // });
        dispatch(setLoginAction(res.data.content));
        navigate('/');
        console.log(res);
      })
      .catch((err) => {
        message.error('Đăng nhập thất bại');
        console.log(err);
      });
  };
  const onFinishThunk = (values) => {
    let handleSuccess = () => {
      message.success('Đăng nhập thành công');
      navigate('/');
    };
    dispatch(setLoginActionService(values, handleSuccess));
  };
  const onFinishFailed = (errorInfo) => {
    console.log('Failed:', errorInfo);
  };
  return (
    <div className="w-screen h-screen p-20 bg-orange-500 flex justify-center items-center">
      <div className="container p-20 bg-white rounded-lg flex">
        <div className="w-1/2 h-full">
          <Lottie animationData={bg_animate} loop={true} />
        </div>
        <div className="w-1/2 h-full">
          <h2>Login Page</h2>
          <Form
            name="basic"
            labelCol={{
              span: 8,
            }}
            wrapperCol={{
              span: 24,
            }}
            className="w-full"
            initialValues={{
              remember: true,
            }}
            onFinish={onFinishThunk}
            onFinishFailed={onFinishFailed}
            autoComplete="off"
            layout="vertical"
          >
            <Form.Item
              label="Username"
              name="taiKhoan"
              rules={[
                {
                  required: true,
                  message: 'Please input your username!',
                },
              ]}
            >
              <Input />
            </Form.Item>
            <Form.Item
              label="Password"
              name="matKhau"
              rules={[
                {
                  required: true,
                  message: 'Please input your password!',
                },
              ]}
            >
              <Input.Password />
            </Form.Item>
            <Form.Item
              wrapperCol={{
                offset: 0,
                span: 24,
              }}
            >
              <Button danger type="primary" htmlType="submit">
                Submit
              </Button>
            </Form.Item>
          </Form>
        </div>
      </div>
    </div>
  );
};
export default LoginPage;
