import { Button, Form, Input, message } from 'antd';
import { useNavigate } from 'react-router-dom';
import { userService } from '../../services/userService';

const LoginPage = () => {
  let navigate = useNavigate();
  const onFinish = (values) => {
    userService
      .login(values)
      .then((res) => {
        message.success('Đăng nhập thành công');
        navigate('/');
        console.log(res);
      })
      .catch((err) => {
        message.error('Đăng nhập thất bại');
        console.log(err);
      });
  };
  const onFinishFailed = (errorInfo) => {
    console.log('Failed:', errorInfo);
  };
  return (
    <div className="w-screen h-screen p-20 bg-orange-500 flex justify-center items-center">
      <div className="container p-20 bg-white rounded-lg">
        <h2>Login Page</h2>
        <Form
          name="basic"
          labelCol={{
            span: 8,
          }}
          wrapperCol={{
            span: 24,
          }}
          className="w-full"
          initialValues={{
            remember: true,
          }}
          onFinish={onFinish}
          onFinishFailed={onFinishFailed}
          autoComplete="off"
          layout="vertical"
        >
          <Form.Item
            label="Username"
            name="taiKhoan"
            rules={[
              {
                required: true,
                message: 'Please input your username!',
              },
            ]}
          >
            <Input />
          </Form.Item>
          <Form.Item
            label="Password"
            name="matKhau"
            rules={[
              {
                required: true,
                message: 'Please input your password!',
              },
            ]}
          >
            <Input.Password />
          </Form.Item>
          <Form.Item
            wrapperCol={{
              offset: 0,
              span: 24,
            }}
          >
            <Button type="primary" htmlType="submit">
              Submit
            </Button>
          </Form.Item>
        </Form>
      </div>
    </div>
  );
};
export default LoginPage;
