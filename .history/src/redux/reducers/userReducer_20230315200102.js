import { SET_USER_LOGIN } from '../constants/userConstant';

const initialState = {
  userInfor: null,
};

let userReducer = (state = initialState, { type, payload }) => {
  switch (type) {
    case SET_USER_LOGIN: {
      return { ...state, userInfor: payload };
    }
    default:
      return state;
  }
};

export default userReducer;
