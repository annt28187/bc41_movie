import { SET_USER_LOGIN } from '../constants/userConstant';
import { userServ } from './../../services/userService';

export const setLoginAction = (values) => ({
  type: SET_USER_LOGIN,
  payload: values,
});
export const setLoginActionService = (values) => {
  return (dispatch) => {
    userServ
      .login(values)
      .then((res) => {
        console.log(res);
        dispatch({
          type: SET_USER_LOGIN,
          payload: res.data.content,
        });
      })
      .then((err) => {
        console.log(err);
      });
  };
};
