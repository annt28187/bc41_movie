import { SET_USER_LOGIN } from '../constants/userConstant';

export const setLoginAction = (values) => ({
  type: SET_USER_LOGIN,
  payload: values,
});
