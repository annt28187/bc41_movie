import { message } from 'antd';
import { localUserServ } from '../../services/localService';
import { SET_USER_LOGIN } from '../constants/userConstant';
import { userServ } from './../../services/userService';

export const setLoginAction = (values) => ({
  type: SET_USER_LOGIN,
  payload: values,
});
let navigate = useNavigate();
export const setLoginActionService = (values) => {
  return (dispatch) => {
    userServ
      .login(values)
      .then((res) => {
        console.log(res);
        message.success('Đăng nhập thành công');
        localUserServ.set(res.data.content);
        navigate('/');
        dispatch({
          type: SET_USER_LOGIN,
          payload: res.data.content,
        });
      })
      .then((err) => {
        console.log(err);
      });
  };
};
