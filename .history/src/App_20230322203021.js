import { BrowserRouter, Route, Routes } from 'react-router-dom';
import './App.css';
import Layout from './Layout/Layout';
import BookingPage from './Pages/BookingPage/BookingPage';
import DetailPage from './Pages/DetailPage/DetailPage';
import HomePage from './Pages/HomePage/HomePage';
import LoginPage from './Pages/LoginPage/LoginPage';
import NotFoundPage from './Pages/NotFoundPage/NotFoundPage';

function App() {
  return (
    <BrowserRouter>
      <Routes>
        <Route path="/" element={<Layout Component={HomePage} />} />
        <Route path="/login" element={<LoginPage />} />
        <Route path="/detail/:id" element={<Layout Component={DetailPage} />} />
        <Route path="/booking/:id" element={<Layout Component={BookingPage} />} />
        <Route path="*" element={<Layout Component={NotFoundPage} />} />
      </Routes>
    </BrowserRouter>
  );
}

export default App;
