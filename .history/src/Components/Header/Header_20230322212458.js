import React from 'react';
import { Desktop, Tablet } from './../../Layout/Responsive';
import HeaderDesktop from './HeaderDesktop';
import HeaderTablet from './HeaderTablet';

export default function Header() {
  return (
    <Desktop>
      <HeaderDesktop />
    </Desktop>
     <Tablet>
      <HeaderTablet />
    </Tablet>
  );
}
