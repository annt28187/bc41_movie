import React from 'react';
import { Desktop } from './../../Layout/Responsive';
import HeaderDesktop from './HeaderDesktop';

export default function Header() {
  return (
    <Desktop>
      <HeaderDesktop />
    </Desktop>
  );
}
