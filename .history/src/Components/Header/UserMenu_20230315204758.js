import React from 'react';
import { useSelector } from 'react-redux';
import { localUserService } from '../../services/localService';
import userReducer from './../../redux/reducers/userReducer';

export default function UserMenu() {
  let userInfo = useSelector((state) => {
    return state.userReducer.userInfo;
  });
  let handleLogout = () => {
    localUserService.remove();
    window.location.reload();
    // window.location.href = '/login';
  };
  let renderContent = () => {
    if (userInfo) {
      <>
        <span>{userInfo.hoTen}</span>
        <button onClick={handleLogout} className="px-5 rounded border-black-50 border-2">
          Đăng xuất
        </button>
      </>;
    } else {
      <>
        <button className="px-5 rounded border-black-50 border-2">Đăng nhập</button>
        <button className="px-5 rounded border-black-50 border-2">Đăng ký</button>
      </>;
    }
  };
  return <div className="space-x-5">{renderContent()}</div>;
}
