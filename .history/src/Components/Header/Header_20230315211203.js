import React from 'react';
import UserMenu from './UserMenu';
import ListMovie from './../../Pages/HomePage/ListMovie/ListMovie';

export default function Header() {
  return (
    <div className="h-20 show">
      <div className="container mx-auto flex justify-between items-center h-full">
        <span className="font-bold text-red-500 text-2xl">CyberFix</span>
        <UserMenu />
        <ListMovie />
      </div>
    </div>
  );
}
