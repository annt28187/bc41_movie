import React from 'react';
import UserMenu from './UserMenu';

export default function Header() {
  return (
    <div className="h-20 show">
      <div className="container mx-auto flex justify-between items-center h-full">
        <span className="font-bold text-red-500 text-2xl">CyberFix</span>
        <UserMenu />
      </div>
    </div>
  );
}
