import React from 'react';
import UserMenu from './UserMenu';

export default function Header() {
  return (
    <div className="h-20 show">
      <div className="container mx-auto">
        <span>CyberFix</span>
        <UserMenu />
      </div>
    </div>
  );
}
