import moment from 'moment/moment';
import React from 'react';

export default function ItemTabMovie({ phim }) {
  return (
    <div className="flex items-center space-x-5 border-black border-b pb-3">
      <img src={phim.hinhAnh} className="w-28 h-48 object-cover object-top rounded" alt="" />
      <div>
        <h5 className="text-xl font-medium mb-5">{phim.tenPhim}</h5>
        <div className="gird gap-5 gird-cols-3">
          {phim.lstLichChieuTheoPhim.slice(0, 6).map((item) => {
            return (
              <span className="rounded p-3 text-white bg-red-500 font-medium">
                {moment(item.ngayChieuGioChieu).format('dd- MM-yyyy ~ hh-mm')}
              </span>
            );
          })}
        </div>
      </div>
    </div>
  );
}
