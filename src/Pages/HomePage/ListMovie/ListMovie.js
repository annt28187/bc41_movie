import React, { useEffect, useState } from 'react';
import { movieServ } from '../../../services/movieService';
import ItemMovie from './ItemMovie';

export default function ListMovie() {
  const [movieList, setMoiveList] = useState([]);
  useEffect(() => {
    movieServ
      .getMovieList()
      .then((res) => {
        console.log(res.data.content);
        setMoiveList(res.data.content);
      })
      .then((err) => {
        console.log(err);
      });
  });
  return (
    <div className="grid grid-cols-4 gap-10">
      {movieList.slice(0, 8).map((item) => {
        return <ItemMovie data={item} key={item.maPhim} />;
      })}
    </div>
  );
}
