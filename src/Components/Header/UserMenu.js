import React from 'react';
import { useSelector } from 'react-redux';
import { NavLink } from 'react-router-dom';
import { localUserServ } from '../../services/localService';

export default function UserMenu() {
  let userInfo = useSelector((state) => {
    return state.userReducer.userInfo;
  });
  let handleLogout = () => {
    localUserServ.remove();
    window.location.reload();
    // window.location.href = '/login';
  };
  let renderContent = () => {
    if (userInfo) {
      return (
        <>
          <span>{userInfo.hoTen}</span>
          <button onClick={handleLogout} className="px-5 rounded border-black-50 border-2">
            Đăng xuất
          </button>
        </>
      );
    } else {
      return (
        <>
          <NavLink to={'/login'}>
            <button className="px-5 rounded border-black-50 border-2">Đăng nhập</button>
          </NavLink>
          <button className="px-5 rounded border-black-50 border-2">Đăng ký</button>
        </>
      );
    }
  };
  return <div className="space-x-5">{renderContent()}</div>;
}
